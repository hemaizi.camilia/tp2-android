package com.example.uapv1900102.library;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


/*
 *@author HEMAIZI CAMILIA
 */

public class BookActivity extends AppCompatActivity {
    BookDbHelper db;
    Book bookitem ;
    EditText textview1 ;
    EditText textview2 ;
    EditText textview3 ;
    EditText textview4 ;
    EditText textview5 ;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        db = new BookDbHelper(this);
        Intent intent = getIntent();

        /*
         *récuperer l'objet Book envoyer par MainActivity grace a la méthode getParcelableExtra
         */
       bookitem= intent.getParcelableExtra("book");

        textview1 = (EditText) findViewById(R.id.nameBook);
        textview2 = (EditText) findViewById(R.id.editAuthors);
        textview3 = (EditText) findViewById(R.id.editYear);
        textview4 = (EditText) findViewById(R.id.editGenres);
        textview5 = (EditText) findViewById(R.id.editPublisher);


        textview1.setText(bookitem.getTitle());
        textview2.setText(bookitem.getAuthors());
        textview3.setText(bookitem.getYear());
        textview4.setText(bookitem.getGenres());
        textview5.setText(bookitem.getPublisher());

        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          sauvegarder();
                                      }
                                  });

    }

    /*
     * fonction qui insere un nouveau livre si le book est vide avec la méthode addBook
     * sinon elle le modifie avec la méthode updateBook
     * il faut actualiser lapplication afin de voir l'insertion/modification !
     */
    public void sauvegarder() {
        /*
         * afficher un message d'erreur si le titre existe deja dans la base de données
         */
        if (db.Exist(textview1.getText().toString()) == true) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Ajout impossible ")
                    .setMessage("Un livre portant le meme nom existe deja dans la base de données.")
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    })
                    .show();
        } else {

            /*
             * afficher un message d'erreur si le titre n'est pas inserer
             */
            if (textview1.getText().toString().isEmpty()) {
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Sauvegarde impossible ")
                        .setMessage("Le titre du livre doit etre non vide.")
                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        })
                        .show();
            } else {
                if (bookitem.getId() == -1) {

                    Book newbook = new Book();
                    newbook.setTitle(textview1.getText().toString());
                    newbook.setAuthors(textview2.getText().toString());
                    newbook.setYear(textview3.getText().toString());
                    newbook.setGenres(textview4.getText().toString());
                    newbook.setPublisher(textview5.getText().toString());

                 boolean ajout=   db.addBook(newbook);

                    /*
                     * afficher un message pour informer que la requete a etait executer avec succes
                     */
                    if(ajout==true){
                        AlertDialog alertDialog = new AlertDialog.Builder(this)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setMessage("Livre ajouté avec succes !")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent2 = new Intent(BookActivity.this, MainActivity.class);
                                        startActivity(intent2);

                                    }
                                })
                                .show();
                    }
                    else
                    { /*
                     * afficher un message en cas d'echec dajout
                     */
                        AlertDialog alertDialog = new AlertDialog.Builder(this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setMessage("Echec de lajout du livre !")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent2 = new Intent(BookActivity.this, MainActivity.class);
                                        startActivity(intent2);

                                    }
                                })
                                .show();
                    }
                } else {
                    bookitem.setTitle(textview1.getText().toString());
                    bookitem.setAuthors(textview2.getText().toString());
                    bookitem.setYear(textview3.getText().toString());
                    bookitem.setGenres(textview4.getText().toString());
                    bookitem.setPublisher(textview5.getText().toString());

                    int res = db.updateBook(bookitem);

                    /*
                     * afficher un message pour informer que la requete a etait executer avec succes
                     */
                    AlertDialog alertDialog = new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setMessage(res + " Modification enregistré avec succes !")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent2 = new Intent(BookActivity.this, MainActivity.class);
                                    startActivity(intent2);

                                }
                            })
                            .show();
                }


            }
        }
    }
}


