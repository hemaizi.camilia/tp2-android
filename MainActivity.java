package com.example.uapv1900102.library;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


/*
 *@author HEMAIZI CAMILIA
 */
public class MainActivity extends AppCompatActivity {
    SimpleCursorAdapter adapter;
    ListView listeview;
    BookDbHelper db;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new BookDbHelper(MainActivity.this);
        listeview = findViewById(R.id.liste);
        registerForContextMenu(listeview);

        /*
         *@param fab : le button flottant qui permet d'ajouter un livre
         */
        FloatingActionButton fab = findViewById(R.id.fab);

        /*
        * fonction qui permet d'ajouter a la base de données  et recuperer les champs et les afficher dans la listview
         */
        this.affiche();


        /*
         * Afficher les informations dun livre en cliquant sur l'item de la liste view
         */
        listeview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {

                 intent = new Intent(MainActivity.this, BookActivity.class);

                Cursor c = db.fetchAllBooks();
                c.moveToPosition(position);
                Book p = db.cursorToBook(c);

                intent.putExtra("book", p);
                startActivityForResult(intent, 0);

            }

        });

        /*
         * evenement du button flottant qui permet dinserer un nouveau livres (envois un book vide)
         */
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 intent = new Intent(MainActivity.this, BookActivity.class);
                Book p = new Book();
                p.setId(-1);

                intent.putExtra("book", p);
                startActivityForResult(intent, 0);
            }
        });
    }

    /*
     * remplir la base de données avec la méthode insertBooks(populate)
     * récuperer les informations dans la base de données a partir de la méthode fetchAllBooks
     * utiliser un SimpleCursorAdapter pour récuperer le titre et les auteurs et l'adapter avec notre listview
     */
    public void affiche()
    {
        /*
        * executer la premiere ligne qu'une seule fois pour eviter de inserer a chaque lancement de lactivité
         */
        //db.populate();
        Cursor cursor=db.fetchAllBooks();

        if(cursor !=null)
        {
        adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] {db.COLUMN_BOOK_TITLE,db.COLUMN_AUTHORS},
                new int[] {android.R.id.text1,android.R.id.text2 },0);
        listeview.setAdapter(adapter);
        }
    }

/****************************** Question 5 *************************************************************/


    /*
     * evenement click long sur item qui declenche un menu contextuel
     */

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_main, menu);

    }

    /*
     * evenement click sur l'item du menu declanche la suppression du livre
     */

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int index = info.position;

        long id = listeview.getItemIdAtPosition(index);

        Cursor cursor = db.getBookById(id);
        if(cursor !=null) {
            // supression du livre selectionner avec la methode deleteBook
            // il faut actualiser lapplication afin de voir la suppression !
            db.deleteBook(cursor);

            /*
             * afficher un message d'information de suppression reussi
             */
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Supression du livre avec succes !"+cursor.getString(1).toString())
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .show();


            return true;
        }
        return true;

    }





}
