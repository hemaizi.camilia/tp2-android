package com.example.uapv1900102.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;

/**
 ** @author HEMAIZI Camilia
 */

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    /**
     * CONSTRUCTOR
     */
    public BookDbHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * CREATE THE TABLE LIBRARY AND EXECUTE THE QUERY
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + "  INTEGER primary key autoincrement, " +
                COLUMN_BOOK_TITLE + " TEXT not null UNIQUE," +
                COLUMN_AUTHORS + " TEXT not null UNIQUE," +
                COLUMN_YEAR + "  INTEGER not null," +
                COLUMN_GENRES + " TEXT not null," +
                COLUMN_PUBLISHER + " TEXT not null" +")";

        db.execSQL(sql);
        Log.i("database","Database created !!");

    }

    /**
     * UPGRADE VESRION
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.w(BookDbHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);

    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BookDbHelper.COLUMN_BOOK_TITLE, book.getTitle());
        values.put(BookDbHelper.COLUMN_AUTHORS, book.getAuthors());
        values.put(BookDbHelper.COLUMN_GENRES, book.getGenres());
        values.put(BookDbHelper.COLUMN_YEAR, book.getYear());
        values.put(BookDbHelper.COLUMN_PUBLISHER, book.getPublisher());

        long rowID  = db.insert(BookDbHelper.TABLE_NAME, null,
                values);

       db.close();

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res=0;
        String where = BookDbHelper._ID  + " = ?";
        String[] whereValue = {Long.toString(book.getId())};
        ContentValues values = new ContentValues();
        values.put(BookDbHelper.COLUMN_BOOK_TITLE, book.getTitle());
        values.put(BookDbHelper.COLUMN_AUTHORS, book.getAuthors());
        values.put(BookDbHelper.COLUMN_GENRES, book.getGenres());
        values.put(BookDbHelper.COLUMN_YEAR, book.getYear());
        values.put(BookDbHelper.COLUMN_PUBLISHER, book.getPublisher());

        db.update(BookDbHelper.TABLE_NAME,values,where, whereValue);

        Cursor cursor  = db.rawQuery("SELECT changes() AS count", null);
        if(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst())
        {
            res = cursor.getInt(cursor.getColumnIndex("count"));
        }
        return res;
    }



    /**
     * Get all the Books
     */
    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = {COLUMN_BOOK_TITLE,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER,COLUMN_AUTHORS};

        //Cursor cursor = db.query(BookDbHelper.TABLE_NAME, columns, null, null, null, null, null);
        Cursor cursor = db.rawQuery("SELECT * FROM library",null);

        return cursor;
    }


    /**
     * Delete a book
     * @param cursor
     */

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();

        // call db.delete();
        db.delete(BookDbHelper.TABLE_NAME, BookDbHelper._ID + " = " + cursor.getLong(0), null);
        db.close();
    }


    /**
     * insert new books
     */
    public void populate() {
        Log.d(TAG, "call populate()");
        this.addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        this.addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        this.addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        this.addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        this.addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        this.addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        this.addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        this.addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        this.addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        this.addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        this.addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    /**
     * transform a cursor into a Book
     */
    public static Book cursorToBook(Cursor cursor) {
        Book book = new Book();

        book.setId(cursor.getLong(0));
        book.setTitle(cursor.getString(1));
        book.setAuthors(cursor.getString(2));
        book.setYear(cursor.getString(3));
        book.setGenres(cursor.getString(4));
        book.setPublisher(cursor.getString(5));

        return book;
    }

    /*
     * fonction qui retourne le livre(cursor) en fonction de l'id
     */
   public Cursor getBookById(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

       Cursor cursor = db.rawQuery("SELECT * FROM library where _id ="+id,null);

        cursor.moveToFirst();
        return cursor;
    }

    /*
     * fonction qui vérifie si le titre existe deja dans la base de données
     */
    public boolean Exist(String titre) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM library where title ='"+titre+"'",null);

        if(cursor.getCount()>0){
            return true;
        }
        return false;
    }
}
